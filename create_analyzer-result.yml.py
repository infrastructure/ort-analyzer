#!/usr/bin/env python3
# SPDX-License-Identifier: MIT
#
# Copyright Bosch.IO GmbH 2023. All rights reserved, also regarding any disposal, exploitation, reproduction,
# editing, distribution, as well as in the event of applications for industrial property rights.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next paragraph) shall be included in all copies or substantial
# portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
# ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
Script to prove the concept to use Apertis *.img.licenses.gz compliance data
and Debian metadata files to generate an analyzer-result.yml ORT file.
See README for details.
"""

import argparse
import json
import math as m
import requests
import subprocess
import sys
from datetime import *

class Package:
    """ Handle metadata of Apertis packages """

    def __init__(self, name, architecture, version, homepage, filename, sha256, description, plicense, pcopyright):
        self.name         = name
        self.architecture = architecture
        self.version      = version
        self.homepage     = homepage
        self.filename     = filename
        self.sha256       = sha256
        self.description  = description
        self.license      = plicense
        self.copyright    = pcopyright

    def name(self):
        return self.name

parser = argparse.ArgumentParser(
    description="Generate an analyzer-result.yml ORT file"
)
parser.add_argument(
    "--input",
    type=argparse.FileType("r"),
    help="input file in JSON format (e.g. apertis-inputdata.json)",
)
parser.add_argument(
    "--project",
    type=str,
    help="project name (e.g. Apertis-ORT-Demo:1.0.0)",
)
parser.add_argument(
    "--image",
    type=str,
    help="FIXME (e.g. apertis_v2024dev2-hmi-amd64-uefi_v2024dev2.0.img.gz)",
)
parser.add_argument(
    "--release_date",
    type=str,
    help="FIXME (e.g. 2023-06-21)",
)
parser.add_argument(
    "--license_url",
    type=str,
    help="FIXME (e.g. https://images.apertis.org/release/v2024dev2/v2024dev2.0/amd64/hmi/apertis_v2024dev2-hmi-amd64-uefi_v2024dev2.0.img.licenses.gz)",
)
parser.add_argument(
    "--repository",
    type=str,
    help="FIXME (e.g. https://repositories.apertis.org/apertis/)",
)
parser.add_argument(
    "--compliance_data",
    type=str,
    help="FIXME (e.g. Apertis/apertis_v2024dev2-hmi-amd64-uefi_20230621.1604.img.licenses)",
)
parser.add_argument(
    "--status-package",
    type=str,
    help="FIXME (e.g. Apertis/status)",
)
parser.add_argument(
    "--pkglist_url",
    type=str,
    help="FIXME (e.g. https://images.apertis.org/release/v2024dev2/v2024dev2.0/amd64/hmi/apertis_v2024dev2-hmi-amd64-uefi_v2024dev2.0.img.pkglist.gz)",
)
parser.add_argument(
    "--repository-packages",
    type=str,
    action="append",
    help="FIXME (e.g. Apertis/v2024dev2_target_binary-amd64_Packages)",
)
parser.add_argument(
    "--repository-packages-url",
    type=str,
    action="append",
    help="FIXME (e.g. https://repositories.apertis.org/apertis/dists/v2024dev2/target/binary-amd64/Packages)",
)
parser.add_argument(
    "--license_mapping",
    type=str,
    help="license mapping file in JSON format (e.g. license_mapping.json)",
)
parser.add_argument(
    "--result-file",
    type=str,
    help="output file in YAML format (e.g. analyzer-result.yml)",
)
args = parser.parse_args()

start_time = str(datetime.today()).replace(' ', 'T') + 'Z'

packages= []
vp = []

if args.input:
    print(f'input JSON file provided, other arguments will be ignored!')
    ipda = json.load(args.input)
else:
    print(f'input JSON file NOT provided, other arguments will be used!')
    if not args.project:
        sys.exit('--project argument is required!')
    if not args.image:
        sys.exit('--image argument is required!')
    if not args.release_date:
        sys.exit('--release_date argument is required!')
    if not args.license_url:
        sys.exit('--license_url argument is required!')
    if not args.repository:
        sys.exit('--repository argument is required!')
    if not args.compliance_data:# TODO remove, duplicated with license_url
        sys.exit('--compliance_data argument is required!')# TODO remove, duplicated with license_url
    if not args.status_package:# TODO remove, duplicated with pkglist_url
        sys.exit('--status-package argument is required!')# TODO remove, duplicated with pkglist_url
    if not args.pkglist_url:
        sys.exit('--pkglist_url argument is required!')
    if not args.repository_packages:
        sys.exit('--repository-packages argument is required!')
    if not args.repository_packages_url:
        sys.exit('--repository-packages-url argument is required!')
    if not args.license_mapping:
        sys.exit('--license_mapping argument is required!')
    if not args.result_file:
        sys.exit('--result-file argument is required!')
    ipda = {"project": args.project,
            "image": args.image,
            "release_date": args.release_date,
            "license_url": args.license_url,
            "repository": args.repository,
            "compliance_data": args.compliance_data,
            "status-package": args.status_package,
            "repository-packages": args.repository_packages,
            "license_mapping": args.license_mapping,
            "result-file": args.result_file,
    }
    # Download required files
    ## img.licenses.gz
    license = requests.get(args.license_url)
    if license.status_code != 200:
        sys.exit('xxx.img.licenses.gz file not reachable!')
    with open(args.compliance_data + ".gz", "wb") as compliance_data:
        compliance_data.write(license.content)
    subprocess.run(['gunzip', '-f', args.compliance_data + '.gz'])

    ## img.pkglist.gz
    pkglist = requests.get(args.pkglist_url)
    if pkglist.status_code != 200:
        sys.exit('xxx.img.pkglist.gz file not reachable!')
    with open(args.status_package + ".gz", "wb") as status_package:
        status_package.write(pkglist.content)
    subprocess.run(['gunzip', '-f', args.status_package + '.gz'])

    ## Packages files
    for index,repopack in enumerate(args.repository_packages):
        repopack_data = requests.get(args.repository_packages_url[index])
        if repopack_data.status_code != 200:
            sys.exit('Package file not reachable!')
        with open(repopack, "wb") as repopack:
            repopack.write(repopack_data.content)

project = ipda["project"]
image = ipda["image"]
release_date= ipda["release_date"]
license_url = ipda["license_url"]
repository = ipda["repository"]

compliance_data = open(ipda["compliance_data"], 'r')
status = open(ipda["status-package"], 'r')
lrpack = ipda["repository-packages"]
for p in range(len(lrpack)):
    packages.append(open(lrpack[p], 'r'))
result = open(ipda["result-file"], 'w')

license_mapping = open(ipda["license_mapping"], 'r')

tail1 = '      source_artifact:\n        url: ""\n        hash:\n          value: ""\n          algorithm: ""\n'
tail2 = '      vcs:\n        type: ""\n        url: ""\n        revision: ""\n        path: ""\n'
tail3 = '      vcs_processed:\n        type: ""\n        url: ""\n        revision: ""\n        path: ""'
tail = tail1 + tail2 + tail3

status_list = status.readlines()

pdesc = []
pname = parch = pvers = phom = ''
separator = ' AND '

print('read installed packages from "status"!')
for line in status_list:
    if line.find('Package:') != -1:
        pname = line[line.find(': ') + 2:len(line)]
    elif line.find('Architecture:') != -1:
        parch = line[line.find(': ') + 2:len(line)]
    elif line.find('Version:') != -1:
        pvers = line[line.find(': ') + 2:len(line)]
    elif line.find('Description:') != -1:
        pdesc.append(line[line.find(': ') + 2:len(line)])
    elif line[0] == " " and not line[0:2] == ' /':
        pdesc.append(line[line.find(': ') + 2:len(line)])
    elif line.find('Homepage:') != -1:
        phom = line[line.find(': ') + 2:len(line)]
    elif line[0] == '\n':
        vp.append(Package(pname, parch, pvers, phom, '', '', pdesc, '', ''))
        pname = parch = pvers = phom = ''
        pdesc = []

data = json.load(compliance_data)
lstp = data["packages"]
lstl = []
lstc = []
lm = json.load(license_mapping)

print('work on compliance data!')
for obj in vp:
    for p in range(len(lstp)):
        if obj.name.rstrip('\n') == lstp[p]["package_name"]:
            lstlm = []
            lstl = lstp[p]["package_licenses"]
            for l in range(len(lstl)):
                lstlm.append(str(lm.get(lstl[l])))
            obj.license = lstlm
            cl = str(lstl)
            if cl.find('NoSourceInfoFound') < 0 :
                lstc = lstp[p]["package_copyright"]
                obj.copyright = lstc
    
pname = pvers = pfilename = psha = ''

for pack in packages:
    print('search for installation paths in Packages file!')
    pack_list = pack.readlines()

    for line in pack_list:
        if line.find('Package') != -1:
            pname = line[line.find(': ') + 2:len(line)]
        elif line.find('Version') != -1:
            pvers = line[line.find(': ') + 2:len(line)]
        elif line.find('Filename') != -1:
            pfilename = line[line.find(': ') + 2:len(line)]
        elif line.find('SHA256') != -1:
            psha = line[line.find(': ') + 2:len(line)]
        elif line[0] == '\n':
            for obj in vp:
                if obj.name == pname and obj.version == pvers:
                    obj.filename = pfilename
                    obj.sha256 = psha
                    pname = pvers = pfilename = psha = ''

end_time =  str(datetime.today()).replace(' ', 'T') + 'Z'

result.write('---\nrepository:\n  vcs:\n    type: ""\n    url: ""\n    revision: ""\n    path: ""')
result.write('\n  vcs_processed:\n    type: ""\n    url: ""\n    revision: ""\n    path: ""\n  config: {}')
result.write('\nanalyzer:\n  start_time: "' + start_time + '"\n  end_time: "' + end_time +'"')
result.write('\n  environment:\n    ort_version: "65c661208d"\n    java_version: "17.0.6"\n    os: "Linux"\n    processors: 3\n    max_memory: 2545942528')
result.write('\n    variables:\n      SHELL: "/bin/bash"\n      TERM: "xterm-256color"\n    tool_versions: {}')
result.write('\n  config:\n    allow_dynamic_versions: false\n    skip_excluded: false')
result.write('\n    package_managers:')
result.write('\n      DotNet:\n        options:\n          directDependenciesOnly: "true"')
result.write('\n      NuGet:\n        options:\n          directDependenciesOnly: "true"')
result.write('\n  result:\n    projects:\n    - id: "Debian::' + project + '"')
result.write('\n      definition_file_path: "' + image +'"')
result.write('\n      declared_licenses: []\n      declared_licenses_processed: {}')
result.write('\n      vcs:\n        type: ""\n        url: ""\n        revision: ""\n        path: ""')
result.write('\n      vcs_processed:\n        type: ""\n        url: ""\n        revision: ""\n        path: ""')
result.write('\n      homepage_url: "' + repository + image + '"\n      scope_names:\n      - "default"')
result.write('\n    packages:')

for obj in vp:
    result.write('\n    - id: "Debian:' + obj.architecture.rstrip('\n') + ':' + obj.name.rstrip('\n') + ':' + obj.version.rstrip('\n').replace(":", "D") +'"')
    result.write('\n      purl: "pkg:deb/debian/' + obj.name.rstrip('\n') + '@' + obj.version.rstrip('\n').replace(":", "D") + '?distro=bullseye"')
    if len(obj.copyright) > 0:
        result.write('\n      authors:')
        for c in range(len(obj.copyright)):
            text = obj.copyright[c]
            result.write('\n      - "' + text.replace('"', "'") + '"')
    result.write('\n      declared_licenses:')
    if len(obj.license) == 0:
        result.write('\n      - "NONE"')
        result.write('\n      declared_licenses_processed:')
        result.write('\n        spdx_expression: "NONE"')
    else:
        for l in range(len(obj.license)):
            result.write('\n      - "' + obj.license[l] + '"')
        d_license_p = separator.join(obj.license)
        result.write('\n      declared_licenses_processed:')
        result.write('\n        spdx_expression: "' + d_license_p + '"')
    text = ''
    for line in obj.description:
        if line == obj.description[0]:
            if len(obj.description) == 1:
                text = line.rstrip('\n').replace('"', "'") + '"'
            else:
                text = text + line.rstrip('\n').replace('"', "'") + '\\\n'
        elif line == obj.description[-1]:
            text = text + '        \ ' + line.rstrip('\n').replace('"', "'") + '"'
        else:
            text = text + '        \ ' + line.rstrip('\n').replace('"', "'") + '\\\n'
    result.write('\n      description: "' + text)
    result.write('\n      homepage_url: "' + obj.homepage.rstrip('\n') + '"')
    result.write('\n      binary_artifact:')
    result.write('\n        url: "' + repository + obj.filename.rstrip('\n') + '"')
    result.write('\n        hash:')
    result.write('\n          value: "' + obj.sha256.rstrip('\n') + '"')
    result.write('\n          algorithm: "SHA-256"')
    result.write('\n' + tail)

result.write('\n    dependency_graphs:')
result.write('\n      Debian:')
result.write('\n        packages:')
for obj in vp:
    result.write('\n        -  "Debian:' + obj.architecture.rstrip('\n') + ':' + obj.name.rstrip('\n') + ':' + obj.version.rstrip('\n').replace(":", "D") +'"')
result.write('\n        scopes:')
result.write('\n          :' + project + ':default:')
result.write('\n          - root: 0')
i = 0
for obj in vp:
    i = i + 1
    if i > 1:
        result.write('\n          - root: ' + str(i-1))
result.write('\n        nodes:')
result.write('\n        - {}')
i = 0
for obj in vp:
    i = i + 1
    if i > 1:
        result.write('\n        - pkg: ' + str(i-1))
result.write('\n        edges: []')
result.write('\n    has_issues: false')
result.write('\nscanner: null')
result.write('\nadvisor: null')
result.write('\nevaluator: null')

status.close()
compliance_data.close()
license_mapping.close()
for f in packages:
    f.close()
result.close()
