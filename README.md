The OSS Review Toolkit is now integrated in the Apertis images builder pipeline
(see https://gitlab.apertis.org/infrastructure/apertis-image-recipes/-/merge_requests/613).
For each Apertis image generated, an ORT report is provided in the same folder of
the [Apertis image site](https://images.apertis.org/).

As an example, the ORT report of the image `apertis_ostree_v2024pre-fixedfunction-amd64-uefi_20231101.0018.img.gz`
is available at https://images.apertis.org/weekly/v2024pre/20231101.0018/amd64/fixedfunction/
under the name of [apertis_ostree_v2024pre-fixedfunction-amd64-uefi_20231101.0018.img.ort-reports.tar.gz](https://images.apertis.org/weekly/v2024pre/20231101.0018/amd64/fixedfunction/apertis_ostree_v2024pre-fixedfunction-amd64-uefi_20231101.0018.img.ort-reports.tar.gz).
In other words, the report file name is similar to the image file name to which
we add the `.img.ort-reports.tar.gz` ending.

The steps below are provided as an example to reproduce the ORT report generated
by the Apertis image pipeline.

# How to generate a ORT analyzer-result.yml with create_analyzer-result.yml.py

## With a JSON file as input:
```
mkdir Apertis
cd Apertis

wget https://images.apertis.org/weekly/v2024dev2/20230621.1604/amd64/hmi/apertis_v2024dev2-hmi-amd64-uefi_20230621.1604.img.licenses.gz
wget https://images.apertis.org/weekly/v2024dev2/20230621.1604/amd64/hmi/apertis_v2024dev2-hmi-amd64-uefi_20230621.1604.img.pkglist.gz

gunzip apertis_v2024dev2-hmi-amd64-uefi_20230621.1604.img.licenses.gz
gunzip apertis_v2024dev2-hmi-amd64-uefi_20230621.1604.img.pkglist.gz

wget https://repositories.apertis.org/apertis/dists/v2024dev2/target/binary-amd64/Packages -O v2024dev2_target_binary-amd64_Packages
wget https://repositories.apertis.org/apertis/dists/v2024dev2/non-free/binary-amd64/Packages -O v2024dev2_non-free_binary-amd64_Packages

cd ..

./create_analyzer-result.yml.py --input apertis-inputdata.json
```

## Or by given data as arguments:
```
mkdir Apertis

./create_analyzer-result.yml.py \
  --project "Apertis-ORT-Demo:1.0.0" \
  --image "apertis_v2024dev2-hmi-amd64-uefi_v2024dev2.0.img.gz" \
  --release_date "2023-06-21" \
  --license_url "https://images.apertis.org/release/v2024dev2/v2024dev2.0/amd64/hmi/apertis_v2024dev2-hmi-amd64-uefi_v2024dev2.0.img.licenses.gz" \
  --repository "https://repositories.apertis.org/apertis/" \
  --compliance_data "Apertis/apertis_v2024dev2-hmi-amd64-uefi_20230621.1604.img.licenses" \
  --status-package "Apertis/status" \
  --pkglist_url "https://images.apertis.org/release/v2024dev2/v2024dev2.0/amd64/hmi/apertis_v2024dev2-hmi-amd64-uefi_v2024dev2.0.img.pkglist.gz" \
  --repository-packages "Apertis/v2024dev2_target_binary-amd64_Packages" \
  --repository-packages-url "https://repositories.apertis.org/apertis/dists/v2024dev2/target/binary-amd64/Packages" \
  --repository-packages "Apertis/v2024dev2_non-free_binary-amd64_Packages" \
  --repository-packages-url "https://repositories.apertis.org/apertis/dists/v2024dev2/non-free/binary-amd64/Packages" \
  --license_mapping "license_mapping.json" \
  --result-file "Apertis/analyzer-result.yml"
```
This example doesn't work well since it is based on apertis/v2024dev2 which is
known to be affected by issues on packages copyright data [1-2]. Because of the
rebase of Apertis on Debian/Bookworm, a large part of packages doesn't contain
any useful data for copyright that means the final ORT reports are quite empty.

[1] https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/324
[2] https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/360

Instead, we will run the example on apertis/v2024dev3 which provides packages
with all required copyright data.
```
mkdir Apertis

./create_analyzer-result.yml.py \
  --project "Apertis-ORT-Demo:1.0.0" \
  --image "apertis_v2024dev3-hmi-amd64-uefi_v2024dev3.0rc1.img.gz" \
  --release_date "2023-08-30" \
  --license_url "https://images.apertis.org/release/v2024dev3/v2024dev3.0rc1/amd64/hmi/apertis_v2024dev3-hmi-amd64-uefi_v2024dev3.0rc1.img.licenses.gz" \
  --repository "https://repositories.apertis.org/apertis/" \
  --compliance_data "Apertis/apertis_v2024dev3-hmi-amd64-uefi_v2024dev3.0rc1.img.licenses" \
  --status-package "Apertis/status" \
  --pkglist_url "https://images.apertis.org/release/v2024dev3/v2024dev3.0rc1/amd64/hmi/apertis_v2024dev3-hmi-amd64-uefi_v2024dev3.0rc1.img.pkglist.gz" \
  --repository-packages "Apertis/v2024dev3_target_binary-amd64_Packages" \
  --repository-packages-url "https://repositories.apertis.org/apertis/dists/v2024dev3/target/binary-amd64/Packages" \
  --repository-packages "Apertis/v2024dev3_non-free_binary-amd64_Packages" \
  --repository-packages-url "https://repositories.apertis.org/apertis/dists/v2024dev3/non-free/binary-amd64/Packages" \
  --license_mapping "license_mapping.json" \
  --result-file "Apertis/analyzer-result.yml"
```

Or if we want to reproduce provided reports in a closer way, we can generate
them from apertis/v2024dev1 (so before the rebase):
```
mkdir Apertis

./create_analyzer-result.yml.py \
  --project "Apertis-ORT-Demo:1.0.0" \
  --image "apertis_v2024dev1-hmi-amd64-uefi_v2024dev1.0.img.gz" \
  --release_date "2023-03-16" \
  --license_url "https://images.apertis.org/release/v2024dev1/v2024dev1.0/amd64/hmi/apertis_v2024dev1-hmi-amd64-uefi_v2024dev1.0.img.licenses.gz" \
  --repository "https://repositories.apertis.org/apertis/" \
  --compliance_data "Apertis/apertis_v2024dev1-hmi-amd64-uefi_v2024dev1.0.img.licenses" \
  --status-package "Apertis/status" \
  --pkglist_url "https://images.apertis.org/release/v2024dev1/v2024dev1.0/amd64/hmi/apertis_v2024dev1-hmi-amd64-uefi_v2024dev1.0.img.pkglist.gz" \
  --repository-packages "Apertis/v2024dev1_target_binary-amd64_Packages" \
  --repository-packages-url "https://repositories.apertis.org/apertis/dists/v2024dev1/target/binary-amd64/Packages" \
  --repository-packages "Apertis/v2024dev1_non-free_binary-amd64_Packages" \
  --repository-packages-url "https://repositories.apertis.org/apertis/dists/v2024dev1/non-free/binary-amd64/Packages" \
  --license_mapping "license_mapping.json" \
  --result-file "Apertis/analyzer-result.yml"
```

# How to install ORT

ORT doesn't release any version, thus we have to use their git repository.
Unfortunately, the `main` branch is not compatible with our `analyzer-result.yml`
and `disclosure_document_custom.ftl`. To allow a reproducible result, we have
to retrieve a specific commit of ORT (i.e. 65c661208d).

```
git clone https://github.com/oss-review-toolkit/ort
cd ort
git checkout -b apertis  65c661208d

# To add authors as copyright holders in the final reports
# This can be done, in config files but doesn't seem to work correctly.

sed -i 's/addAuthorsToCopyrights: Boolean = false/addAuthorsToCopyrights: Boolean = true/' model/src/main/kotlin/config/OrtConfiguration.kt

DOCKER_BUILDKIT=1 docker build -t ort .
cd ..
```

## Generate an ORT requirements file
```
docker run ort requirements &> Apertis/apertis.ort-requirements.log
```
This doesn't reproduce the provided `ort-requirements.log`, but it allows to
reproducible install an compatible ORT version which generates the expected
final reports without having to install ORT dependencies from different location
(i.e. Debian Bullseye apt repo, python pip repo, upstream git repo or other
random repositories).

## How to publish ORT to the registry
In order to reuse the built docker image in a gitlab-ci pipeline, we must
publish it to the Apertis gitlab registry:
```
DOCKER_BUILDKIT=1 docker build -t ort:65c661208d .
docker login registry.gitlab.apertis.org
docker tag ort:65c661208d registry.gitlab.apertis.org/infrastructure/ort-analyzer:65c661208d
docker push registry.gitlab.apertis.org/infrastructure/ort-analyzer:65c661208d
```

To build and publish a new version of the ORT docker image, the following steps
are required:
```
TAG=1.0.0

git checkout ${TAG}

sed -i 's/addAuthorsToCopyrights: Boolean = false/addAuthorsToCopyrights: Boolean = true/' model/src/main/kotlin/config/OrtConfiguration.kt

DOCKER_BUILDKIT=1 docker build -t ort:${TAG} .
docker login registry.gitlab.apertis.org
docker tag ort:${TAG} registry.gitlab.apertis.org/infrastructure/ort-analyzer:${TAG}
docker push registry.gitlab.apertis.org/infrastructure/ort-analyzer:${TAG}
```
In this example, we built a new docker image based on the recently released
[ORT 1.0.0](https://github.com/oss-review-toolkit/ort/releases/tag/1.0.0).

# How to run ORT with analyzer-result.yml
## PDF report
```
docker run \
       -v $PWD/:/project \
       ort report \
       -f PdfTemplate -i /project/Apertis/analyzer-result.yml -o /project/Apertis/reports \
       --report-option PdfTemplate=template.path=/project/disclosure_document_custom.ftl
```

## Web App
```
docker run \
       -v $PWD/:/project \
       ort report \
       -f WebApp -i /project/Apertis/analyzer-result.yml -o /project/Apertis/reports
```

## SPDX YML
```
docker run \
       -v $PWD/:/project \
       ort report \
       -f SpdxDocument -i /project/Apertis/analyzer-result.yml -o /project/Apertis/reports
```

## CycloneDx XML
```
docker run \
       -v $PWD/:/project \
       ort report \
       -f CycloneDx -i /project/Apertis/analyzer-result.yml -o /project/Apertis/reports
```
